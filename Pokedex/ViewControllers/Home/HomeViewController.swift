//
//  HomeViewController.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/7/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var prefersStatusBarHidden: Bool {
      return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func postAdTapped() {
        let cameraViewController = CameraViewController.instantiate()
        cameraViewController.maxImageCount = Constant.maxUploadImage
        cameraViewController.photoSelectedHandler = { [unowned self] photos in
            self.dismiss(animated: false, completion: nil)
            let postAdViewController = PostAdViewController.instantiate()
            postAdViewController.photos = photos
            let navigationController =  UINavigationController(rootViewController: postAdViewController)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: false, completion: nil)
        }
        cameraViewController.modalPresentationStyle = .fullScreen
        self.present(cameraViewController, animated: true, completion: nil)
    }
}
