//
//  GalleryViewController.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit
import Photos

class GalleryViewController: BaseViewController {

    var startingImageCount: Int = 0
    var maxImageCount: Int = 0
    
    let minimumLineSpacing: CGFloat = 4.0
    let minimumInteritemSpacing: CGFloat = 4.0
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var fetchResult: PHFetchResult<PHAsset>!
    private var cachingImageManager: PHCachingImageManager!
    private var selectedIndexPaths = [Int]()
    
    var photoSelectedHandler: (([PHAsset]) -> ())?
    
    private lazy var rightBarButtonItem: UIBarButtonItem = {
        return UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneTapped))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
}

extension GalleryViewController {
    
    private var size: CGSize {
        let numberOfColumns: CGFloat = 3.0
        let width = (self.view.frame.size.width - (numberOfColumns - 1) * minimumLineSpacing) / numberOfColumns
        
        return CGSize(width: width, height: width)
    }
    
    private var thumbnailSize: CGSize {
        let scale: CGFloat = 2.0
        
        return CGSize(width: self.size.width * scale, height: self.size.height * scale)
    }
}

extension GalleryViewController: PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        if let _ = changeInstance.changeDetails(for: self.fetchResult) {
            self.collectionView.reloadData()
        }
    }
}

extension GalleryViewController {
    
    private func setupFetchResult() {
        PHPhotoLibrary.shared().register(self)
        
        self.cachingImageManager = PHCachingImageManager()
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        //fetchOptions.fetchLimit = 1
        self.fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
    }
    
    private func setupCollectionView() {
        if let collectionViewFlowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.minimumLineSpacing = minimumLineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = minimumInteritemSpacing
        }
    }
    
    private func setupBarButtonItems() {
        let leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTapped))
        let rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneTapped))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    override func setupElements() {
        super.setupElements()
        self.setupCollectionView()
        self.setupBarButtonItems()
        self.setupFetchResult()
        self.updateDoneButton()
    }
}

extension GalleryViewController {
    
    @objc func doneTapped() {
        guard self.selectedIndexPaths.count > 0 else { return }
        self.photoSelectedHandler?(self.selectedIndexPaths.map { self.fetchResult.object(at: $0) })
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func cancelTapped() {
        self.photoSelectedHandler?([])
        self.dismiss(animated: true, completion: nil)
    }
    
    private func updateDoneButton() {
        self.rightBarButtonItem.isEnabled = self.selectedIndexPaths.count > 0
    }
}

extension GalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fetchResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.identifier, for: indexPath) as! GalleryCollectionViewCell
        let access = self.fetchResult.object(at: indexPath.item)
        self.cachingImageManager.requestImage(for: access, targetSize: self.thumbnailSize, contentMode: .aspectFit, options: nil) { (image, _) in
            cell.thumbnailImageView.image = image
        }
        
        cell.selectedNumber = 0
        if let index = self.selectedIndexPaths.firstIndex(of: indexPath.item) {
            cell.selectedNumber = index + 1 + self.startingImageCount
        }
        
        return cell
    }
}

extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.size
    }
}

extension GalleryViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = self.selectedIndexPaths.firstIndex(of: indexPath.item) {
            self.selectedIndexPaths.remove(at: index)
            if let cell = self.collectionView.cellForItem(at: indexPath) as? GalleryCollectionViewCell {
                cell.selectedNumber = 0
            }
            for case let cell as GalleryCollectionViewCell in self.collectionView.visibleCells {
                if let indexPath = self.collectionView.indexPath(for: cell) {
                    if let index = self.selectedIndexPaths.firstIndex(of: indexPath.item) {
                        cell.selectedNumber = index + 1 + self.startingImageCount
                    }
                }
            }
        } else {
            guard self.startingImageCount + self.selectedIndexPaths.count < self.maxImageCount else {
                return
            }
            
            self.selectedIndexPaths.append(indexPath.item)
            if let cell = self.collectionView.cellForItem(at: indexPath) as? GalleryCollectionViewCell {
                cell.selectedNumber = self.selectedIndexPaths.count + self.startingImageCount
            }
        }
        self.updateDoneButton()
    }
}
 
