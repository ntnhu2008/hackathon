//
//  CategoryViewController.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/7/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class CategoryViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var categories = ["Car", "Motobik", "Laptop", "Phone"]
    
    override func setupElements() {
        super.setupElements()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "dismiss_btn")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(dismissTapped))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        self.navigationItem.title = "Categories"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func dismissTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CategoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension CategoryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = self.categories[indexPath.row]
        
        return cell
    }
}
