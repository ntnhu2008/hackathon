//
//  PostAdViewController.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit
import Photos
import Alamofire

class PostAdViewController: BaseViewController {

    let photoSection: Int = 0
    let headerSection: Int = 1
    let categorySection: Int = 2
    let subCategorySection: Int = 3
    let priceSection: Int = 4
    let subjectSection: Int = 5
    let bodySection: Int = 6
    let dynamicSection: Int = 7
    
    @IBOutlet weak var tableView: UITableView!
    
    var dynamicParams = [DynamicParam]()
    var categoryName: String?
    var subCategoryName: String?
    
    var photos = [PHAsset]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uploadImages()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PostAdViewController {
    
    private func dataList() -> [Data] {
        var datas = [Data]()
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        for asset in self.photos {
            PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: requestOptions) { (image, _) in
                if let image = image {
                    let imageData = image.jpegData(compressionQuality: 0.5)!
                    
                    datas.append(imageData)
                }
            }
        }

        return datas
    }
    
    private func uploadImages() {
        
        var headers = [String: String]()
        headers["Content-type"] = "multipart/form-data"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (index, data) in self.dataList().enumerated() {
                let currentTime = "\(Date().timeIntervalSince1970)\(index)"
                let fileName = "\(currentTime).jpg"
                multipartFormData.append(data, withName: fileName, mimeType: "image/jpg")
            }
        }, usingThreshold: UInt64.init(),
           to: "http://34.73.34.47:5000/upload",
           method: .post,
           headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseString { response in
                    debugPrint(response)
                    switch response.result {
                    case .success(let response):
                        self.handleJSONString(text: response)
                        self.tableView.reloadData()
                    case .failure(let error):
                        break
                    }
                }
            case .failure(let encodingError):
                break
            }
        })
    }
}

struct DynamicParam {
    var title: String
    var name: String?
}

extension PostAdViewController {
    func handleJSONString(text: String) {
        if let data = text.data(using: .utf8) {
            do {
                var json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                if let category = json["cat"] as? String {
                    self.categoryName = category
                    json.removeValue(forKey: "cat")
                }
                
                if let subCategory = json["subcat"] as? String {
                    self.subCategoryName = subCategory
                    json.removeValue(forKey: "subcat")
                }
                
                for key in json.keys {
                    let name = json[key] as? String
                    let dynamic = DynamicParam(title: key, name: name)
                    self.dynamicParams.append(dynamic)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

extension PostAdViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case photoSection:
            return 1
        case headerSection:
            return 1
        case categorySection:
            return 1
        case subCategorySection:
            return 1
        case priceSection:
            return 1
        case subjectSection:
            return 1
        case bodySection:
            return 1
        case dynamicSection:
            return self.dynamicParams.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case photoSection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: PhotoTableViewCell.identifier) as! PhotoTableViewCell
            cell.photos = self.photos
            return cell
        case headerSection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.identifier) as! HeaderTableViewCell
            
            return cell
        case categorySection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: SingleChoiceTableViewCell.identifier) as! SingleChoiceTableViewCell
            cell.titleLabel.text = "Category"
            cell.nameLabel.text = self.categoryName
            
            return cell
        case subCategorySection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: SingleChoiceTableViewCell.identifier) as! SingleChoiceTableViewCell
            cell.titleLabel.text = "Sub Category"
            cell.nameLabel.text = self.subCategoryName
            
            return cell
            
        case priceSection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: InputTableViewCell.identifier) as! InputTableViewCell
            cell.titleLabel.text = "Price"
            
            return cell
        case subjectSection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: InputTableViewCell.identifier) as! InputTableViewCell
            cell.titleLabel.text = "Title"
            
            return cell
        case bodySection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: BodyTableViewCell.identifier) as! BodyTableViewCell
            cell.titleLabel.text = "Description"
            
            return cell
        case dynamicSection:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: SingleChoiceTableViewCell.identifier) as! SingleChoiceTableViewCell
            let dynamic = self.dynamicParams[indexPath.row]
            
            cell.titleLabel.text = dynamic.title
            cell.nameLabel.text = dynamic.name
            
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension PostAdViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case categorySection:
            let categoryViewController = CategoryViewController.instantiate()
            let navigationController = UINavigationController(rootViewController: categoryViewController)
            self.present(navigationController, animated: true, completion: nil)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case photoSection:
            return 120.0
        case headerSection:
            return 59.0
        case categorySection,
             subCategorySection,
             priceSection,
             subjectSection:
            return 80.0
        case bodySection:
            return 160.0
        case dynamicSection:
            return 80.0
        default:
            return 0.0
        }
    }
}

extension PostAdViewController {
    
    private func setupTableView() {
        self.tableView.register(UINib(nibName: SingleChoiceTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: SingleChoiceTableViewCell.identifier)
        self.tableView.register(UINib(nibName: InputTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: InputTableViewCell.identifier)
        self.tableView.register(UINib(nibName: BodyTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: BodyTableViewCell.identifier)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 60))
        footerView.backgroundColor = .clear
        
        self.tableView.tableFooterView = footerView
        //self.tableView.backgroundColor = UIColor(rgb: 0xFFBA00)
    }
    
    override func setupElements() {
        super.setupElements()
        self.setupTableView()
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "dismiss_btn")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(dismissTapped))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        self.navigationItem.title = "New Ad"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xFFBA00)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
}

extension PostAdViewController {
    
    @objc func dismissTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
