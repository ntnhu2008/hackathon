//
//  Constant.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import Foundation

struct Constant {
    static let maxUploadImage: Int = 6
}
