//
//  ViewController.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/1/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import Photos

class CameraViewController: BaseViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var maxImageCount: Int = 0
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var toggleButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var thumbnailView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var imageCountButton: ActionButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ppLookLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var modelJson = [String: Any]()
    
    var photoSelectedHandler: (([PHAsset]) -> ())?
    
    private var isCapturing: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.captureButton.isEnabled = !self.isCapturing
            }
        }
    }
    
    var selectedAssets = [PHAsset]()
    
    // MARK: - Vision Properties
    var request: VNCoreMLRequest?
    var visionModel: VNCoreMLModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.loadData()
        self.loadJson()
        
        self.nameLabel.text = nil
        self.ppLookLabel.text = nil
        self.priceLabel.text = nil
        self.categoryLabel.text = nil
    }
    
    func setupCaptureSession() {
        // create a new capture session
        
        // find the available cameras
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        
        guard let captureDevice = availableDevices.first else { return }
        do {
            try captureDevice.lockForConfiguration()
            captureDevice.activeVideoMinFrameDuration = CMTimeMake(value: 1, timescale: Int32(15.0))
            captureDevice.activeVideoMaxFrameDuration = CMTimeMake(value: 1, timescale: Int32(15.0))
            captureSession.addInput(try AVCaptureDeviceInput(device: captureDevice))
        } catch {
            // print an error if the camera is not available
            print(error.localizedDescription)
        }
        
        // Photo image output
        stillImageOutput = AVCapturePhotoOutput()
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
        
        // setup the video output to the screen and add output to our capture session
        let captureOutput = AVCaptureVideoDataOutput()
        
        let width = self.view.frame.size.width
        let height = self.view.frame.size.height
        let size = min(width, height) - 25 * 2
        
        let videoSettings = [AVVideoScalingModeKey: AVVideoScalingModeFit,
        kCVPixelBufferWidthKey as String: size,
        kCVPixelBufferHeightKey as String: size] as [String : Any]
        //captureOutput.videoSettings = videoSettings
        
        captureSession.addOutput(captureOutput)
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.frame = self.view.frame
        containerView.layer.addSublayer(previewLayer)
        
        // buffer the video and start the capture session
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        
        self.startCamera()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // load our CoreML Pokedex model
//        guard let model = try? VNCoreMLModel(for: YOLOv3Tiny().model) else { return }
//
//        // run an inference with CoreML
//        let request = VNCoreMLRequest(model: model) { (finishedRequest, error) in
//
//            // grab the inference results
//            guard let results = finishedRequest.results as? [VNClassificationObservation] else { return }
//
//            // grab the highest confidence result
//            guard let Observation = results.first else { return }
//
//            // create the label text components
//            let predclass = "\(Observation.identifier)"
//            let predconfidence = String(format: "%.02f%", Observation.confidence * 100)
//
//            // set the label text
//            DispatchQueue.main.async(execute: {
//                self.categoryLabel.text = "\(predclass) \(predconfidence)"
//            })
//        }
        
        // create a Core Video pixel buffer which is an image buffer that holds pixels in main memory
        // Applications generating frames, compressing or decompressing video, or using Core Image
        // can all make use of Core Video pixel buffers
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        
//        // execute the request
//        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
        
        let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer)
        try? handler.perform([request!])
    }
}

extension CameraViewController {
    
    func startCamera() {
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
        }
    }
    
    func stopCamera() {
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.stopRunning()
        }
    }
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation() else { return }
        guard let image = UIImage(data: imageData) else  { return }
        self.thumbnailImageView.image = image
        
        var placeHolder: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges({
            let changeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            placeHolder = changeRequest.placeholderForCreatedAsset
        }) { (success, error) in
            if success, let placeHolder = placeHolder {
                let fetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [placeHolder.localIdentifier], options: nil)
                if let asset = fetchResult.firstObject {
                    self.selectedAssets.append(asset)
                }
            }
            self.isCapturing = false
            self.updatePostAdButton()
        }
    }
}

extension CameraViewController {
    
    private func loadJson() {
        if let path = Bundle.main.path(forResource: "model", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let jsonResult = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any] {
                    self.modelJson.merge(jsonResult) { (current, _) in current }
                }
                
            } catch {
                // handle error
            }
        }
    }
    
    var thumbnailSize: CGSize {
        let size = self.thumbnailImageView.frame.size
        let scale: CGFloat = 2.0
        
        return CGSize(width: size.width * scale, height: size.height * scale)
    }
    
    private func updatePostAdButton() {
        DispatchQueue.main.async {
            if self.selectedAssets.count > 0 {
                UIView.performWithoutAnimation {
                    self.imageCountButton.setTitle("Post \(self.selectedAssets.count)/\(self.maxImageCount)", for: .normal)
                    self.imageCountButton.layoutIfNeeded()
                }
                self.imageCountButton.backgroundColor = UIColor(rgb: 0xFE9900)
                self.imageCountButton.layer.borderColor = UIColor(rgb: 0xFE9900).cgColor
            } else {
               UIView.performWithoutAnimation {
                self.imageCountButton.setTitle("\(self.selectedAssets.count)/\(self.maxImageCount)", for: .normal)
                    self.imageCountButton.layoutIfNeeded()
                }
                self.imageCountButton.backgroundColor = .clear
                self.imageCountButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
}

extension CameraViewController {
    
    @IBAction func dismissTapped() {
        self.photoSelectedHandler?([])
        self.stopCamera()
    }
    
    @IBAction func captureTapped() {
        guard self.selectedAssets.count < self.maxImageCount else { return }
        
        self.isCapturing = true
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func postTapped() {
        self.photoSelectedHandler?(self.selectedAssets)
        self.stopCamera()
    }
    
    @objc func thumbnailTapped() {
        self.stopCamera()
        let galleryViewController = GalleryViewController.instantiate()
        galleryViewController.photoSelectedHandler = { [unowned self] (photos) in
            self.selectedAssets.append(contentsOf: photos)
            self.updatePostAdButton()
            self.startCamera()
        }
        
        galleryViewController.maxImageCount = self.maxImageCount
        galleryViewController.startingImageCount = self.selectedAssets.count
        let navigationController = UINavigationController(rootViewController: galleryViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func toggleCameraTapped() {
        //Indicate that some changes will be made to the session
        guard let currentCameraInput: AVCaptureInput = self.captureSession.inputs.first else {
            return
        }
        
        defer {
            self.toggleButton.isEnabled = true
            self.captureSession.commitConfiguration()
        }
        self.toggleButton.isEnabled = false
        self.captureSession.beginConfiguration()
        
        //Get new input
        var newCaptureDeviceInput: AVCaptureDevice?
        if let input = currentCameraInput as? AVCaptureDeviceInput {
            if (input.device.position == .back) {
                newCaptureDeviceInput = cameraWithPosition(position: .front)
            } else {
                newCaptureDeviceInput = cameraWithPosition(position: .back)
            }
        }
        
        guard let newCamera = newCaptureDeviceInput else { return }
        //Add input to session
        var error: NSError?
        var newVideoInput: AVCaptureDeviceInput?
        do {
            newVideoInput = try AVCaptureDeviceInput(device: newCamera)
        } catch let err1 as NSError {
            error = err1
        }

        if let videoInput = newVideoInput {
            self.captureSession.removeInput(currentCameraInput)
            self.captureSession.addInput(videoInput)
        } else if let error = error {
            print("Error creating capture device input: \(error.localizedDescription)")
        }
    }
    
    private func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .unspecified)
        for device in discoverySession.devices {
            if device.position == position {
                return device
            }
        }

        return nil
    }
    
    @IBAction func controlFlashTapped() {
       
    }
}

extension CameraViewController {
    
    private func loadLastPhoto() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchOptions.fetchLimit = 1

        // Fetch the image assets
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        guard fetchResult.count > 0 else { return }
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = false

        // Perform the image request
        PHImageManager.default().requestImage(for: fetchResult.object(at: 0) as PHAsset, targetSize: self.thumbnailSize, contentMode: .aspectFill, options: requestOptions) { (image, _) in
            self.thumbnailImageView.image = image
        }
    }
}

// MARK: - Setup Core ML
extension CameraViewController {
    func setupModel() {
        if let visionModel = try? VNCoreMLModel(for: YOLOv3Int8LUT().model) {
            self.visionModel = visionModel
            request = VNCoreMLRequest(model: visionModel, completionHandler: visionRequestDidComplete)
            //request?.imageCropAndScaleOption = .scaleFill
        } else {
            fatalError("fail to create vision model")
        }
    }
}
// MARK: - Post-processing
extension CameraViewController {
    func visionRequestDidComplete(request: VNRequest, error: Error?) {
        if let predictions = request.results as? [VNRecognizedObjectObservation] {
            //  TODO
            
            let sorts = predictions.sorted { (ob1, ob2) -> Bool in
                return ob1.confidence > ob2.confidence
            }
            
            guard let Observation = sorts.first else {
                DispatchQueue.main.async(execute: {
                    self.nameLabel.text = nil
                    self.ppLookLabel.text = nil
                    self.priceLabel.text = nil
                    self.categoryLabel.text = nil
                })
                return
            }

            let predclass = "\(Observation.labels.first!.identifier)"
            //guard Observation.confidence >= 0.5 else { return }
            //let predconfidence = String(format: "%.02f%", Observation.confidence * 100)
            
            // set the label text
            DispatchQueue.main.async(execute: {
                if let dict = self.modelJson[predclass] as? [String: Any] {
                    self.nameLabel.text = predclass.uppercased()
                    self.ppLookLabel.text = dict["pp_look"] as? String
                    self.priceLabel.text = dict["price"] as? String
                    self.categoryLabel.text = dict["category"] as? String
                }
            })
        }
    }
}

extension CameraViewController {
    
    private func loadData() {
        self.loadLastPhoto()
    }
    
    override func setupElements() {
        super.setupElements()
        self.setupModel()
        self.setupCaptureSession()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(thumbnailTapped))
        self.thumbnailView.addGestureRecognizer(tapGestureRecognizer)
        
        self.captureView.layer.borderWidth = 4.0
        self.captureView.layer.cornerRadius = self.captureView.frame.size.width / 2.0
        self.captureView.layer.borderColor = UIColor.white.cgColor
        
        self.thumbnailImageView.layer.borderWidth = 1.0
        self.thumbnailImageView.layer.cornerRadius = 8.0
        self.thumbnailImageView.layer.borderColor = UIColor.white.cgColor
        
        
        self.imageCountButton.padding = 15.0
        self.imageCountButton.layer.cornerRadius = self.imageCountButton.frame.size.height / 2.0
        self.imageCountButton.layer.borderWidth = 1.0
        self.imageCountButton.layer.borderColor = UIColor.white.cgColor
        self.imageCountButton.layer.masksToBounds = true
    }
}

class ActionButton: UIButton {
    
    var padding: CGFloat = 0.0
    
    override var intrinsicContentSize: CGSize {
        let baseSize = super.intrinsicContentSize
        return CGSize(width: baseSize.width + padding, height: baseSize.height)
    }
}
