//
//  ImageViewController.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/4/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    var image: UIImage?
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = self.image
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ImageViewController {
    
    @IBAction func dismissTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
