//
//  PhotoCollectionViewCell.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: BaseCollectionViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.thumbnailImageView.layer.cornerRadius = 8.0
    }
}
