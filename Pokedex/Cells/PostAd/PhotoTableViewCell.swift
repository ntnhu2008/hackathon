//
//  PhotoTableViewCell.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit
import Photos

class PhotoTableViewCell: BaseTableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    private var cachingImageManager: PHCachingImageManager!
    
    var photos = [PHAsset]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cachingImageManager = PHCachingImageManager()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension PhotoTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.photos.count < Constant.maxUploadImage {
            return self.photos.count + 1
        }
        
        return self.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item < self.photos.count {
            
            let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
            let asset = self.photos[indexPath.item]
            self.cachingImageManager.requestImage(for: asset, targetSize: CGSize(width: 160.0, height: 160.0), contentMode: .aspectFit, options: nil) { (image, _) in
                cell.thumbnailImageView.image = image
            }
            
            return cell
        }
        
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: AddPhotoCollectionViewCell.identifier, for: indexPath) as! AddPhotoCollectionViewCell
        
        return cell
    }
}

extension PhotoTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80.0, height: 80.0)
    }
}
