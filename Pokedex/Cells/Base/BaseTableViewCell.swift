//
//  BaseTableViewCell.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
