//
//  BaseCollectionViewCell.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
