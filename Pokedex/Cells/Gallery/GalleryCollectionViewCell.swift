//
//  GalleryCollectionViewCell.swift
//  Pokedex
//
//  Created by Nhu Nguyen on 11/6/19.
//  Copyright © 2019 Nhu Nguyen. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: BaseCollectionViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var selectedLabel: UILabel!
    
    var selectedNumber: Int = 0 {
        didSet {
            self.selectedLabel.isHidden = self.selectedNumber <= 0
            self.selectedLabel.text = "\(String(describing: selectedNumber))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedLabel.layer.cornerRadius = self.selectedLabel.frame.size.width / 2.0
    }
}
